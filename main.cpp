#include "stdio.h"
#include <iostream>
using namespace std;

int main() {
  cout << "Hello World";
  // user-input-branch
  int number;
  cout << "Enter an integer :";
  cin >> number;
  cout << "Square of you number is ::" << number * number;
  return 0;
}
